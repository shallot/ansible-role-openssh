# OpenSSH Ansible Role

This is an [Ansible](https://docs.ansible.com/ansible/latest/) role for
managing [OpenSSH](https://www.openssh.com/) client and server resources.
